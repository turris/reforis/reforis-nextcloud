# Swedish translations for reForis NextCloud Plugin.
# Copyright (C) 2024 CZ.NIC, z.s.p.o. (https://www.nic.cz/)
# This file is distributed under the same license as the reForis NextCloud
# Plugin project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: reForis NextCloud Plugin 0.0.1\n"
"Report-Msgid-Bugs-To: tech.support@turris.cz\n"
"POT-Creation-Date: 2024-10-09 15:18+0200\n"
"PO-Revision-Date: 2023-09-02 10:52+0000\n"
"Last-Translator: Kristoffer Grundström "
"<swedishsailfishosuser@tutanota.com>\n"
"Language: sv\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/turris"
"/reforis-nextcloud-plugin/sv/>\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.16.0\n"

#: js/src/app.js:14 js/src/nextcloud/Nextcloud.js:40
msgid "Nextcloud"
msgstr "Nextcloud"

#: js/src/nextcloud/ConfigurationForm.js:67
msgid "Username"
msgstr "Användarnamn"

#: js/src/nextcloud/ConfigurationForm.js:76
msgid "Password"
msgstr "Lösenord"

#: js/src/nextcloud/ConfigurationForm.js:88
msgid "Save"
msgstr "Spara"

#: js/src/nextcloud/ConfigurationForm.js:97
msgid "Username cannot be empty."
msgstr "Användarnamn kan inte vara tomt."

#: js/src/nextcloud/ConfigurationForm.js:98
msgid "Password cannot be empty."
msgstr "Lösenord kan inte vara tomt."

#: js/src/nextcloud/Nextcloud.js:42
msgid ""
"Nextcloud puts your data at your fingertips, under your control. Store "
"your documents, calendar, contacts and photos on a server at home."
msgstr ""
"Nextcloud har din data till hands, under din kontroll. Lagra dina "
"dokument, kalender, kontakter och foton på en server hemma."

#: js/src/nextcloud/Nextcloud.js:47
msgid ""
"Before installing and configuring Nextcloud, it's important to first set "
"up external storage, as using the router's internal flash memory can "
"cause it to wear out quickly and affect performance. For more "
"information, see the "
msgstr ""

#: js/src/nextcloud/Nextcloud.js:55
msgid "documentation"
msgstr ""

#: js/src/nextcloud/Nextcloud.js:64
msgid ""
"For the best results, it's recommended to use an external drive or SSD. "
"You can easily configure external storage by using the "
msgstr ""

#: js/src/nextcloud/Nextcloud.js:68
msgid "Storage plugin"
msgstr ""

#: js/src/nextcloud/Nextcloud.js:121
msgid "Configuring Nextcloud…"
msgstr "Ställer in Nextcloud…"

#: js/src/nextcloud/Nextcloud.js:127
msgid "Congratulations!🎉"
msgstr "Gratulerar!🎉"

#: js/src/nextcloud/Nextcloud.js:129
msgid "You have configured your Nextcloud and now you can visit the "
msgstr ""

#: js/src/nextcloud/Nextcloud.js:137
msgid "site"
msgstr ""

#: js/src/nextcloud/Nextcloud.js:156
msgid "Nextcloud Configuration"
msgstr "Inställning av Nextcloud"

#: js/src/nextcloud/Nextcloud.js:157
#, fuzzy
msgid "Create an administrator account for Nextcloud."
msgstr "Skapa ett administratör-konto för Nextcloud."

#: reforis_nextcloud/__init__.py:46
msgid "Cannot configure Nextcloud"
msgstr "Kan inte ställa in Nextcloud"

#~ msgid ""
#~ "You have configured your Nextcloud now"
#~ " you can visit the <a "
#~ "href=\"/nextcloud\" target=\"_blank\" rel=\"noopener "
#~ "noreferrer\">site<sup><i class=\"fas fa-external-"
#~ "link-alt fa-sm ms-1\"></i></sup></a>."
#~ msgstr ""
#~ "Du har ställt in ditt Nextcloud så"
#~ " nu kan du besöka <a "
#~ "href=\"/nextcloud\" target=\"_blank\" rel=\"noopener "
#~ "noreferrer\">hemsidan<sup><i class=\"fas fa-"
#~ "external-link-alt fa-sm "
#~ "ml-1\"></i></sup></a>."

